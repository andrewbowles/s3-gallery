const paths = {
  templates: './src/templates/',
  partials: './src/partials/',
  components: './src/components/',
  sass: './src/sass/',
  scripts: './src/js/',
  data: './src/data/'
};

const s3 = {
  apiVersion: '2006-03-01',
  bucket: 'aab-photo-albums',
  region: 'us-west-1'
}

module.exports.paths = paths;
module.exports.s3 = s3;