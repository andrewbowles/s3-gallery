# S3 Gallery SSG
This is a static site that displays photo galleries based on data from an AWS S3 bucket. It is built from the [Static Site Boilerplate](https://github.com/strangemethod/static-site-boilerplate/) by Andrew Bowles.

## Install and Run
- `npm install`
- `npm start`

## Pull Data from S3
To pull data from your S3 bucket and write it to `src/data/s3.json`:
- Add S3 config data to `/src/config.json`
- `npm run data-pull`

## JSON Data
- All JSON data in `/src/data/*.json` is available to handlebars templates and partials

##License
MIT © [Andrew Bowles](https://github.com/strangemethod)