const gulp = require('gulp');
const CONFIG_ = require('../config');
const AWS = require('aws-sdk');
const jsonfile = require('jsonfile')
const fs = require('fs');

const PATHS = {
  s3File: './src/data/s3.json',
  s3MetaFile: './src/data/s3-meta.json'
}


/*
 * Pull S3 data from AWS SDK.
 */

const getS3Data = () => {
  let listObjectsPromise = s3.listObjects({'Bucket': CONFIG_.s3.bucket}).promise();

  let s3Data = listObjectsPromise.then(function(data) {
    return data;
  }).catch(function(err) {
    console.log(err);
  });

  return s3Data;
}

gulp.task('data', async () => {
  AWS.config.update({region: CONFIG_.s3.region});
  s3 = new AWS.S3({apiVersion: CONFIG_.s3.apiVersion});

  // Get S3 data.
  let formattedData = {};
  const s3Data = await getS3Data();
  const bucketPath = 'https://' + CONFIG_.s3.bucket + '.s3-' + CONFIG_.s3.region + '.amazonaws.com/';
  const imageTypes = ['jpg','jpeg','png','gif'];

  // Get existing metadata to avoid overwriting it.
  const rawMetaData = fs.existsSync(PATHS.s3MetaFile) ? fs.readFileSync(PATHS.s3MetaFile) : null;
  let metaData = rawMetaData ? JSON.parse(rawMetaData) : {};

  // Reformat data by directory.
  s3Data.Contents.forEach((s3File) =>{
    const fileName = s3File.Key;
    const fileDir = fileName.split('/')[0];
    const fileSegments = fileName.split('.');
    const fileExt = fileSegments[fileSegments.length - 1];

    if (imageTypes.includes(fileExt)) {
      // Create s3 json object.
      if (!formattedData[fileDir]) formattedData[fileDir] = {};
      formattedData[fileDir][fileName] = {};
      formattedData[fileDir][fileName].imagePath = bucketPath + fileName;

      // Create s3 meta json object.
      if (!metaData[fileDir]) metaData[fileDir] = {};
      if (!metaData[fileDir][fileName]) metaData[fileDir][fileName] = {};
      if (!metaData[fileDir][fileName].imagePath) metaData[fileDir][fileName].imagePath = bucketPath + fileName;
      if (!metaData[fileDir][fileName].imageFormat) metaData[fileDir][fileName].imageFormat = '';
    }
  });

  jsonfile.writeFile(PATHS.s3File, formattedData, { spaces: 2 }, function (err) {
    if (err) console.error(err)
  });

  jsonfile.writeFile(PATHS.s3MetaFile, metaData, { spaces: 2 }, function (err) {
    if (err) console.error(err)
  });
});
