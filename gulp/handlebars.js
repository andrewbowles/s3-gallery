var gulp = require('gulp');
var data = require('gulp-data');
var hb = require('gulp-hb');
var rename = require('gulp-rename');
var layouts = require('handlebars-layouts');
var livereload = require('gulp-livereload');
var lr = require('tiny-lr');
var server = lr();
var path = require('path');
var CONFIG_ = require('../config');
var argv = require('yargs').argv;

/*
 * Compile handlebars templates
 */

gulp.task('handlebars', function () {
  if (argv.editor) {
    console.log('Running local server in edit mode');
  }

  var hbStream = hb()
    .partials(path.join(CONFIG_.paths.partials, '*.hbs'))
    .partials(path.join(CONFIG_.paths.components, '**/*.hbs'))
    .data(path.join(CONFIG_.paths.data, '*.json'))
    .data({'edit_mode': argv.editor})
    .helpers(layouts);

    return gulp
      .src(path.join(CONFIG_.paths.templates, '**/*.hbs'))
      .pipe(hbStream)
      .pipe(rename({
        extname: ""
       }))
      .pipe(rename({
        extname: ".html"
       }))
      .pipe(gulp.dest('./public'))
      .pipe(livereload(server));
});